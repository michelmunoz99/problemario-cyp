#include <stdio.h>
int main(int argc, char const *argv[]) {
  int a = 0;
  int b = 0;

  printf("Ingrese dos números enteros a sumar:");
  scanf("%d %d", &a, &b);
  printf("la suma es:%d+%d=%d\n", a, b, a + b);

  printf("Ingrese dos números enteros a restar:");
  scanf("%d %d", &a, &b);
  printf("La resta es: %d-%d=%d\n", a, b, a - b);

  printf("Ingrese dos números enteros a multiplicar:");
  scanf("%d %d", &a, &b);
  printf("la multiplicación es: %d*%d=%d\n", a, b, a * b);

  printf("Ingrese dos números enteros a Dividir:");
  scanf("%d %d", &a, &b);
  printf("La división es igual a: %d/%d=%d\n", a, b, a / b);

  return 0;
}
