#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main(int argc, char const *argv[]) {
  float a=3.0f,b=2.0f,c=2.0f;
  float x1,x2,resultado,i1,i2;

  resultado=pow(b,2.0)-4.00*a*c;

  if (resultado>0) {
    printf("Las raíces son reales\n");

    x1=((-b+sqrt(resultado))/(2.0*a));
    x2=((-b-sqrt(resultado))/(2.0*a));

    printf("El resultado es: x1 = %.2f y x2= %.2f\n",x1,x2);

  }
  if (resultado==0) {
    printf("Ambas soluciones son iguales\n");

    x1 =(-b)/(2*a);
    x2=x1;
    printf("El resultado es: x1 =%.2f\n",x1);

  }
    if (resultado<0) {
      printf("Las raíces son imaginarias\n");

      i1=(-b/(2.0*a));
               i2=(sqrt((-1)*resultado)/(2.0*a));
               printf("\nLa raíz real es %.2f y la imaginaria es %.2f\n",i2, i1);

    }

  return 0;
}
