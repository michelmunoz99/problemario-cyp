#include <stdio.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  float arista=0.0f;

    printf("\nIngrese la arista:");
    scanf("%f", &arista);

    while (arista<=0)
    {
        printf("\nERREOR: La arista debe ser mayor que cero");
        printf("\nIngrese arista:");
        scanf("%f", &arista);
    }

    printf("\nEl área del cubo es: %.2f",pow( arista, 2 ));

  return 0;
}
