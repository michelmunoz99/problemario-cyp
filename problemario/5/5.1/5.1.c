#include <stdio.h>
int main(int argc, char const *argv[]) {
  int a = 0;

  printf("Ingrese el año: ");
  scanf("%d", &a );

  if ( a % 4 == 0 && a % 100 != 0 || a % 400 == 0 )
      printf( "%d es año bisiesto", a );

  else
      printf( "%d no es año bisiesto", a );

  return 0;
}
