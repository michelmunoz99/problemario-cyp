#include <stdio.h>
#define PI 3.1416
int main(int argc, char const *argv[]) {
  float radio = 0;

  printf("Ingrese el radio: ");
  scanf("%f",&radio );
  while (radio<=0) {
    printf("Error: El radio debe ser >= 0\n" );
    printf("Ingrese el radio: ");
    scanf("%f",&radio );
  }
  if (radio>0) {
    printf("El área es: %.02f\n",(radio*radio)*PI);

  }
  return 0;
}
