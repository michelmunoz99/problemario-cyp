#include <stdio.h>
int main(int argc, char const *argv[]) {
  int v1=15;
  int v2=20;

  printf("v1 ocupa: %lu bytes en memoria\n",sizeof(v1));
  printf("v2 ocupa: %lu bytes en memoria\n",sizeof(v2));
  printf("main ocupa: %lu bytes en memoria\n",sizeof(main));
  printf("argc ocupa: %lu bytes en memoria\n",sizeof(argc));
  printf("argv ocupa: %lu bytes en memoria\n",sizeof(*argv));

  return 0;
}
