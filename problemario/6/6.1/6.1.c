#include <stdio.h>
int main(int argc, char const *argv[]) {
  float calif = 0;
  
  printf("Ingrese la calificación: ");
  scanf("%f",&calif );
  while (calif<0 || calif>10) {
    printf("ERROR: Nota incorrecta, debe ser >= 0 y <= 10\n" );
    printf("Ingrese la calificación: ");
    scanf("%f",&calif );
  }
  if (calif<6) {
    printf("REPROBADO\n");
  } else {
    printf("APROBADO\n");
  }
  return 0;
}
